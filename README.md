# Workspace x86

Versión ultra-simplificada del [workspace](https://github.com/pridolfi/workspace)
para poder compilar y ejecutar programas en C en una plataforma x86.


## Uso

* Realizar un *fork* de este repositorio en su cuenta o grupo de GitLab según
corresponda haciendo clic en el botón `Fork` que está en la parte superior
derecha de la pantalla.

* Clonar el repositorio creado a su PC. Desde una terminal:

```
~ $ git clone https://gitlab.com/<usuario o grupo>/workspace_x86.git && cd workspace_x86
```

* Para compilar y ejecutar un programa, por ejemplo `hola_mundo`:

```
~/workspace_x86 $ cd hola_mundo/
```

```
~/workspace_x86/hola_mundo $ make
*** compiling C file src/main.c ***
gcc -Iinc -Wall -c src/main.c -o out/obj/main.o

*** linking ejecutable.bin ***
gcc out/obj/main.o -o out/ejecutable.bin
```

```
~/workspace_x86/hola_mundo $ make run
Hola mundo!
```

* Para subir cambios al repositorio, por ejemplo luego de modificar
`hola_mundo/src/main.c` para que imprima `Chau mundo!`:

```
~/workspace_x86 $ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   hola_mundo/src/main.c

no changes added to commit (use "git add" and/or "git commit -a")
```

```
~/workspace_x86 $ git add --all
```

```
~/workspace_x86 $ git commit -m "Modifico el mensaje a imprimir"
```

```
~/workspace_x86 $ git push
```

* Si el repositorio está *forkeado* en un grupo de GitLab, los otros integrantes
deben actualizar sus repositorios locales ejecutando:

```
~/workspace_x86 $ git pull
remote: Enumerating objects: 9, done.
remote: Counting objects: 100% (9/9), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 5 (delta 3), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (5/5), done.
From https://gitlab.com/td2-frh-gx/workspace_x86
   26efdfb..4f62760  master     -> origin/master
Updating 26efdfb..4f62760
Fast-forward
 hola_mundo/src/main.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
```
