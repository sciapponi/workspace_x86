/*==================[inclusions]=============================================*/

#include "fsm_garaje.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_EGRESO_SEG  5

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

static FSM_GARAJE_STATES_T state;

static bool evSensor1_On_raised;
static bool evSensor2_On_raised;
static bool evSensor2_Off_raised;
static bool evTick1seg_raised;
static uint8_t count_seg;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    evSensor1_On_raised = 0;
    evSensor2_On_raised = 0;
    evSensor2_Off_raised = 0;
    evTick1seg_raised = 0;
}

/*==================[external functions definition]==========================*/

void fsm_garaje_init(void)
{
    state = REPOSO;
    clearEvents();
}

void fsm_garaje_runCycle(void)
{
    // El diagrama se encuentra en fsm_garaje/media/fsm_garaje_diagrama.png
    switch (state) {
        case REPOSO:
            if (evSensor2_On_raised) {
                hw_ActivarAlarma();
                state = ALARMA;
            }
            else if (evSensor1_On_raised) {
                hw_AbrirBarrera();
                state = INGRESANDO;
            }
            break;

        case INGRESANDO:
            if (evSensor2_On_raised) {
                count_seg = 0;
                state = ESPERANDO_EGRESO;
            }
            break;

        case ESPERANDO_EGRESO:
            if (evSensor1_On_raised) {
                state = INGRESANDO;
            }
            else if (evTick1seg_raised && (count_seg < ESPERA_EGRESO_SEG)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_EGRESO_SEG)) {
                hw_CerrarBarrera();
                state = REPOSO;
            }
            break;

        case ALARMA:
            if (evSensor2_Off_raised) {
                hw_ApagarAlarma();
                state = REPOSO;
            }
            break;
    }

    clearEvents();
}

void fsm_garaje_raise_evSensor1_On(void)
{
    evSensor1_On_raised = 1;
}

void fsm_garaje_raise_evSensor2_On(void)
{
    evSensor2_On_raised = 1;
}

void fsm_garaje_raise_evSensor2_Off(void)
{
    evSensor2_Off_raised = 1;
}

void fsm_garaje_raise_evTick1seg(void)
{
    evTick1seg_raised = 1;
}

void fsm_garaje_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

/*==================[end of file]============================================*/
